const fs = require('fs')
const request = require('request')

const urls = []
const download = function (uri, filename, callback) {
  const r = request({
    url: uri,
    headers: {
      'accept': 'image/*',
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36'
    }
  }).pipe(fs.createWriteStream(filename))
  r.on('error', function (err) { console.log(err) })
  r.on('close', callback)
}

urls.forEach(function (url, index) {
  download(url, `${index}.png`, function () {
    console.log('Done downloading..')
  })
})
